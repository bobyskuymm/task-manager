<h1 class="mt-5">Создание задачи</h1>

<?php foreach ($errors as $error): ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>Ошибка!</strong> <?php echo $error; ?>.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php endforeach; ?>

<form method="post" action="/task/add">
    <div class="form-group">
        <label for="userName">Имя пользователя</label>
        <input type="text" name="userName" class="form-control" id="userName">
    </div>
    <div class="form-group">
        <label for="email">Email</label>
        <input type="text" name="email" class="form-control" id="email">
    </div>
    <div class="form-group">
        <label for="taskText">Текст задачи</label>
        <input type="text" name="taskText" class="form-control" id="task">
    </div>
    <button type="submit" class="btn btn-primary">Сохранить задачу</button>
</form>