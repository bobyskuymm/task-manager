<?php

namespace App\Controller;

use App\Core\Controller as BaseController;
use App\Model\AdminModel;
use App\Model\TaskModel;

/**
 * Class TaskController
 * @package App\Controller
 */
class AdminController extends BaseController
{
    /**
     *
     */
    public function loginAction()
    {
        $errors = [];

        if (count($_POST) && (empty($_POST['login']) || empty($_POST['password']))) {
            $errors[] = "Все поля должны быть заполнены";
        }

        if (count($_POST) && !count($errors)) {
            $model = new AdminModel();
            $login = htmlspecialchars($_POST['login']);
            $password = htmlspecialchars($_POST['password']);
            if ($model->login($login, $password)) {
                header('Location: /');
            } else {
                $errors = ['Введенные данные не верные'];
            }
        }

        $this->view->renderTemplate('admin/login.php', ['errors' => $errors]);
    }

    public function logoutAction()
    {
        $model = new AdminModel();
        $model->logout();
        header('Location: /');
    }
}
