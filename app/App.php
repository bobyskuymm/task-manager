<?php

namespace App;

use App\Controller\AdminController;
use App\Controller\TaskController;
use App\Core\View;
use App\Controller\HomeController;
use App\Model\AdminModel;

/**
 * Class App
 * @package App
 */
class App
{
    private $config;

    /**
     * @var App|null
     */
    private static $instance = null;

    /**
     * @return App|null
     */
    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __clone()
    {
    }

    private function __construct()
    {
    }


    /**
     * @param $config
     */
    public function run($config)
    {
        $this->config = $config;
        $this->dispatchRoute();
    }

    /**
     * @return mixed
     */
    public function getAdminPassword()
    {
        return $this->config['admin']['password'];
    }

    /**
     * @return mixed
     */
    public function getAdminName()
    {
        return $this->config['admin']['login'];
    }

    /**
     * @return bool
     */
    public function isAdminLoggedIn()
    {
        return !empty($_SESSION[AdminModel::AUTH_SESSION_KEY]);
    }

    protected function dispatchRoute()
    {
        $controllerName = 'home';
        $actionName = 'indexAction';

        $uri = str_replace('index.php', '', $_SERVER['REQUEST_URI']);

        if ($uri == "/") {
            $uri = "";
        }

        if (strpos($uri, '?') !== false) {
            $uri = explode('?', $uri);
            $uri = $uri[0];
        }

        $routes = explode('/', $uri);

        if (!empty($routes[1])) {
            $controllerName = $routes[1];
        }

        if (!empty($routes[2])) {
            $actionName = $routes[2] . 'Action';
        }

        $controller = null;
        if ($controllerName == 'home') {
            $controller = new HomeController();
        }

        if ($controllerName == 'task') {
            $controller = new TaskController();
        }

        if ($controllerName == 'admin') {
            $controller = new AdminController();
        }

        if (!empty($controller) && method_exists($controller, $actionName)) {
            $controller->$actionName();
        } else {
            View::errorPageNotFound();
        }
    }

    /**
     * @return \PDO
     */
    public function getDatabaseConnection()
    {
        $host = $this->config['db']['host'];
        $db = $this->config['db']['dbname'];
        $user = $this->config['db']['user'];
        $pass = $this->config['db']['password'];

        $options = [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::ATTR_EMULATE_PREPARES => false,
        ];
        $dsn = "mysql:host=$host;dbname=$db";
        try {
            $pdo = new \PDO($dsn, $user, $pass, $options);
        } catch (\PDOException $e) {
            throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }

        return $pdo;
    }

    protected function loadConfig()
    {
        $this->config = require_once 'app/config/Config.php';
    }
}
