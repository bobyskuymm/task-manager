<?php
namespace App\Model;

use App\App;

/**
 * Class AdminModel
 * @package App\Model
 */
class AdminModel
{
    const AUTH_SESSION_KEY = 'auth_session_key';

    /**
     * @param $name
     * @param $password
     * @return bool
     */
    public function login($name, $password)
    {
        $result = false;
        if($name === App::getInstance()->getAdminName()
            && $password === App::getInstance()->getAdminPassword()
        ) {
            $_SESSION[self::AUTH_SESSION_KEY] = true;
            $result = true;
        }
        return $result;
    }

    public function logout()
    {
        unset($_SESSION[self::AUTH_SESSION_KEY]);
    }
}
