<?php

namespace App\Controller;

use App\Core\Controller as BaseController;
use App\Model\TaskModel;

/**
 * Class HomeController
 * @package App\Controller
 */
class HomeController extends BaseController
{
    /**
     *
     */
    public function indexAction()
    {
        $page = 1;
        if (!empty($_GET['page'])) {
            $page = htmlspecialchars($_GET['page']);
        }

        $task = new TaskModel();

        $sortField = 'id';
        if (!empty($_GET['sort']) && in_array($_GET['sort'], $task->getAvailableSortingFields())) {
            $sortField = htmlspecialchars($_GET['sort']);
        }

        $sortDirection = 'desc';
        if (!empty($_GET['dir']) && in_array($_GET['dir'], $task->getAvailableDirection())) {
            $sortDirection = htmlspecialchars($_GET['dir']);
        }

        $negativeSortDirection = 'asc';
        if ($sortDirection == 'asc') {
            $negativeSortDirection = 'desc';
        }

        $page = 1;
        if (!empty($_GET['page'])) {
            $page = htmlspecialchars($_GET['page']);
        }

        $list = $task->getList($page, $sortField, $sortDirection);

        $taskCount = $task->getCount();
        $totalPage = $taskCount / TaskModel::ITEM_PER_PAGE;
        if ($taskCount !== ($totalPage * TaskModel::ITEM_PER_PAGE)) {
            $totalPage++;
        }

        $this->view->renderTemplate('home/index.php', [
            'list' => $list,
            'totalPage' => $totalPage,
            'currentPage' => $page,
            'sortField' => $sortField,
            'sortDirection' => $sortDirection,
            'negativeSortDirection' => $negativeSortDirection,
            'defaultDirection' => 'asc'
        ]);
    }
}
