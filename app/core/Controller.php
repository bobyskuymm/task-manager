<?php
namespace App\Core;

class Controller
{
    /**
     * @var View
     */
    protected $view;

    public function init()
    {
        $this->view = new View();
    }

    public function __construct()
    {
        $this->init();
    }
}