<?php
namespace App\Core;

use App\App;

class View
{
    protected $layout = 'layout.php';

    public function setLayout($layout)
    {
        $this->layout = $layout;
    }

    public function renderTemplate($view, $data = null)
    {
        if(is_array($data)) {
            extract($data);
        }

        $isAdmin = App::getInstance()->isAdminLoggedIn();

        if(!empty($_SESSION['alert'])) {
            $alert = $_SESSION['alert'];
            unset($_SESSION['alert']);
        }

        require_once 'app/view/' . $this->layout;
    }

    public function setAlert($mess) {
        $_SESSION['alert'] = $mess;
    }

    public static function errorPageNotFound()
    {
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:'.$host.'404');
        die;
    }

    public static function showHomePage()
    {
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('Location:'.$host);
        die;
    }
}
