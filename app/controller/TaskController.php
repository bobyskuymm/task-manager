<?php

namespace App\Controller;

use App\App;
use App\Core\Controller as BaseController;
use App\Core\View;
use App\Model\TaskModel;

/**
 * Class TaskController
 * @package App\Controller
 */
class TaskController extends BaseController
{
    /**
     *
     */
    public function addAction()
    {
        $errors = [];

        if (count($_POST) && (
                empty($_POST['email'])
                || empty($_POST['taskText'])
                || empty($_POST['userName']))
        ) {
            $errors[] = "Все поля должны быть заполнены";
        }

        if (!empty($_POST['email']) && !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $errors[] = "Email не валиден";
        }

        if (count($_POST) && !count($errors)) {
            $model = new TaskModel();
            $userName = htmlspecialchars($_POST['userName']);
            $email = htmlspecialchars($_POST['email']);
            $taskText = htmlspecialchars($_POST['taskText']);
            $model->add($userName, $email, $taskText);
            $this->view->setAlert('Задача успешно создана.');
            View::showHomePage();
        }

        $this->view->renderTemplate('task/add.php', ['errors' => $errors]);
    }

    public function editAction()
    {
        if(!App::getInstance()->isAdminLoggedIn()) {
            header('Location: /');
        }

        $errors = [];
        if(!empty($_GET['id'])) {
            $id = htmlspecialchars($_GET['id']);
        } else {
            View::errorPageNotFound();
        }

        $taskModel = new TaskModel();
        $task = $taskModel->getTask($id);

        if(!$task) {
            View::errorPageNotFound();
        }

        if (count($_POST) &&  empty($_POST['taskText'])
        ) {
            $errors[] = "Заполните поле текст задачи";
        }

        if (count($_POST) && !count($errors)) {
            $taskText = htmlspecialchars($_POST['taskText']);
            $edited = 0;
            if($taskText != $task['taskText']) {
                $edited = 1;
            } else {
                $edited = $task['edited'];
            }
            if(!empty($_POST['status']) && $_POST['status'] == 'on') {
                $status = 1;
            } else {
                $status = 0;
            }
            $taskModel->update($id, $taskText, $status, $edited);
            $this->view->setAlert('Задача успешно сохранена.');
            View::showHomePage();
        }

        $this->view->renderTemplate('task/edit.php',[
            'errors' => $errors,
            'task' => $task
        ]);

    }
}
