<h1 class="mt-5">Редактирование задачи</h1>

<?php foreach ($errors as $error): ?>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>Ошибка!</strong> <?php echo $error; ?>.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php endforeach; ?>

<form method="post" action="/task/edit?id=<?php echo $task['id']; ?>">
    <div class="form-group">
        <label for="taskText">Текст задачи</label>
        <input type="text" name="taskText" class="form-control" id="task" value="<?php echo $task['taskText']; ?>">
    </div>
    <div class="form-group form-check">
        <input type="checkbox" class="form-check-input" name="status"
               id="status" <?php if (!empty($task['status'])): ?> checked <?php endif; ?>>
        <label class="form-check-label" for="status">Выполнена</label>
    </div>

    <button type="submit" class="btn btn-primary">Сохранить задачу</button>
</form>
