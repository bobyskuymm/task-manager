<?php
namespace App\Model;

use App\App;

/**
 * Class TaskModel
 * @package App\Model
 */
class TaskModel
{
    const ITEM_PER_PAGE = 3;
    protected $table = 'tasks';

    protected $availableSortingFields = ['userName', 'email','status'];
    protected $availableDirection = ['asc', 'desc'];

    /**
     * @return array
     */
    public function getAvailableSortingFields()
    {
        return $this->availableSortingFields;
    }

    /**
     * @return array
     */
    public function getAvailableDirection()
    {
        return $this->availableDirection;
    }

    /**
     * @param $userName
     * @param $email
     * @param $taskText
     */
    public function add($userName, $email, $taskText)
    {
        $pdo = App::getInstance()->getDatabaseConnection();

        $sql = "INSERT INTO {$this->table} (userName, email, taskText, status, edited) VALUES (?,?,?,0,0)";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$userName, $email, $taskText]);

    }

    /**
     * @param $id
     * @param $userName
     * @param $email
     * @param $taskText
     * @param null $status
     */
    public function update($id, $taskText, $status, $edited)
    {
        $pdo = App::getInstance()->getDatabaseConnection();
        $sql = "UPDATE {$this->table} SET taskText=?, status=?, edited=? WHERE id=?";
        $stmt= $pdo->prepare($sql);
        $stmt->execute([$taskText, $status, $edited, $id]);
    }

    /**
     * @param $page
     * @param string $sortField
     * @param string $sortDirection
     * @return mixed
     */
    public function getList($page , $sortField, $sortDirection)
    {
        $limit = self::ITEM_PER_PAGE;
        $offset = $limit * ($page-1);
        $pdo = App::getInstance()->getDatabaseConnection();

        return $pdo->query("SELECT * FROM {$this->table} ORDER BY {$sortField} {$sortDirection} "
            . "LIMIT {$limit} OFFSET {$offset}"
        )->fetchAll();
    }

    /**
     * @param $id
     * @return array
     */
    public function getTask($id)
    {
        $pdo = App::getInstance()->getDatabaseConnection();

        return $pdo->query("SELECT * FROM {$this->table} WHERE id={$id}")->fetch();
    }

    /**
     * @param $page
     * @param string $sortField
     * @param string $sortDirection
     * @return mixed
     */
    public function getCount()
    {
        $pdo = App::getInstance()->getDatabaseConnection();

        $result =  $pdo->query("SELECT COUNT(*) as ctn FROM {$this->table} ")->fetch();
        return $result['ctn'];
    }
}
