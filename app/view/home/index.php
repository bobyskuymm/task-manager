<h1 class="mt-5">Список задач <a class="btn btn-primary" href="/task/add" role="button">Добавить</a></h1>

<?php if (!empty($alert)): ?>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <?php echo $alert; ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
<?php endif; ?>

<table class="table">
    <thead>
    <tr>
        <th scope="col">#</th>

        <th scope="col">
            <?php
            $url = "/home/index?page={$currentPage}&sort=userName&dir=";
            if ($sortField == 'userName') {
                $url .= $negativeSortDirection;
            } else {
                $url .= $defaultDirection;
            }
            ?>
            <a href="<?php echo $url ?>">Имя пользователя</a>
        </th>

        <th scope="col">
            <?php
            $url = "/home/index?page={$currentPage}&sort=email&dir=";
            if ($sortField == 'email') {
                $url .= $negativeSortDirection;
            } else {
                $url .= $defaultDirection;
            }
            ?>
            <a href="<?php echo $url; ?>">Email</a>
        </th>

        <th scope="col">
            Статус
        </th>

        <th scope="col">
            Текст задачи
        </th>

        <?php if ($isAdmin): ?>
            <th scope="col">

            </th>
        <?php endif; ?>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($list as $item): ?>
        <tr>
            <th scope="row"><?php echo $item['id']; ?></th>
            <td>
                <?php echo $item['userName']; ?>
                <?php if (!empty($item['edited'])): ?>
                    <span class="badge badge-pill badge-warning">отредактировано администратором</span>
                <?php endif; ?>
            </td>
            <td><?php echo $item['email']; ?></td>
            <td>
                <?php if (!empty($item['status'])): ?>
                    <span class="badge badge-pill badge-success">выполнено</span>
                <?php endif; ?>
            </td>
            <td><?php echo $item['taskText']; ?></td>
            <?php if ($isAdmin): ?>
                <td>
                    <a href="/task/edit?id=<?php echo $item['id']; ?>">Редактировать</a>
                </td>
            <?php endif ?>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<nav aria-label="Page navigation example">
    <ul class="pagination">
        <?php for ($p = 1; $p <= $totalPage; $p++): ?>
            <li class="page-item <?php if ($p == $currentPage): ?> active <?php endif; ?>">
                <?php $url = "/home/index?page={$p}&sort={$sortField}&dir={$sortDirection}" ?>
                <a class="page-link" href="<?php echo $url; ?>">
                    <?php echo $p; ?>
                </a>
            </li>
        <?php endfor; ?>
    </ul>
</nav>
